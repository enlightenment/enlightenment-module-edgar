# This python file use the following encoding: utf-8

import time

import e

from efl import ecore
from efl import evas
from efl import edje
from efl import elementary as elm
from efl.evas import EXPAND_BOTH, EXPAND_HORIZ, FILL_BOTH


__gadget_name__ = 'Network Speed Monitor'
__gadget_vers__ = '0.2'
__gadget_auth__ = 'DaveMDS'
__gadget_mail__ = 'dave@gurumeditation.it'
__gadget_desc__ = 'Network speed monitor'
__gadget_vapi__ = 2
__gadget_opts__ = { 'popup_on_desktop': False }


# import sys
# def DBG(msg):
#    print("NETSPEED: %s" % msg)
#    sys.stdout.flush()


class Gadget(e.Gadget):

    def __init__(self):
        # DBG("init")
        super().__init__()
        self.poller = None
        self.last_in = 0
        self.last_out = 0
        self.last_time = 0
        self.kb_in = 0
        self.kb_out = 0
        self.in_perc = 0
        self.out_perc = 0

    def instance_created(self, obj, site):
        super().instance_created(obj, site)
        # DBG("instance_created (insts: %d  popups:%d)" % (len(self._instances), len(self._popups)))
        obj.size_hint_aspect = evas.EVAS_ASPECT_CONTROL_BOTH, 16, 16

        if self.poller is None:
            self.poller = ecore.Poller(16, self.poller_cb, ecore.ECORE_POLLER_CORE)
            self.last_time = time.time()

    def instance_destroyed(self, obj):
        super().instance_destroyed(obj)
        # DBG("instance_destroyed (insts: %d  popups:%d)" % (len(self._instances), len(self._popups)))
        if len(self._instances) < 1 and self.poller is not None:
            self.poller.delete()
            self.poller = None

    def popup_created(self, elm_parent):
        # DBG("popup_created (insts: %d  popups:%d)" % (len(self._instances), len(self._popups)))

        box = elm.Box(elm_parent)
        box.show()

        in_label = elm.Label(box, size_hint_expand=EXPAND_HORIZ, text="asd")
        box.pack_end(in_label)
        in_label.show()

        out_label = elm.Label(box, size_hint_expand=EXPAND_HORIZ, text="asd")
        box.pack_end(out_label)
        out_label.show()

        box.data['in_label'] = in_label
        box.data['out_label'] = out_label

        self.popup_update(box)
        return box

    def popup_destroyed(self, obj):
        # DBG("popup_destroyed (insts: %d  popups:%d)" % (len(self._instances), len(self._popups)))
        pass

    def popup_update(self, popup):
        popup.data['in_label'].text = 'Recv: %.2f KB/s' % self.kb_in
        popup.data['out_label'].text = 'Trans: %.2f KB/s' % self.kb_out

    def poller_cb(self):
        byte_per_second_in, byte_per_second_out = self.parse_proc()

        self.kb_in = byte_per_second_in / 1000.0
        self.kb_out = byte_per_second_out / 1000.0

        self.in_perc = int(self.kb_in / 1000 * 100)  # TODO CONFIGURABLE MAX
        self.out_perc = int(self.kb_out / 1000 * 100)  # TODO CONFIGURABLE MAX

        for obj in self._instances:
            obj.message_send(1, (0, self.in_perc, 0,  0, self.out_perc, 0))

        for popup in self._popups:
            self.popup_update(popup)

        return ecore.ECORE_CALLBACK_RENEW

    def parse_proc(self):
        tot_in = tot_out = indiff = outdiff = 0
        for line in open("/proc/net/dev", "r").readlines():
            if ':' in line:
                vals = line.split()
                tot_in += int(vals[1])
                tot_out += int(vals[9])

        curtime = time.time()
        timediff = curtime - self.last_time
        self.last_time = curtime

        if self.last_in > 0:
            indiff = tot_in - self.last_in
        self.last_in = tot_in

        if self.last_out > 0:
            outdiff = tot_out - self.last_out
        self.last_out = tot_out

        # DBG("TIME: %s" % timediff)
        # DBG("TOT in:%d  out:%d" % (tot_in, tot_out))
        # DBG("DIFF in:%d  out:%d" % (indiff, outdiff))

        byte_per_second_in = indiff / timediff
        byte_per_second_out = outdiff / timediff

        return byte_per_second_in, byte_per_second_out
